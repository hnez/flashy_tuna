use core::time::Duration;

use crate::tuners;
use crate::buttons;

use tuners::ActiveTuner;
use buttons::Pressed;

pub struct Ui {
    tuners: tuners::Tuners,
    buttons: buttons::Buttons,

}

impl Ui {
    pub fn new(mut tuners: tuners::Tuners, buttons: buttons::Buttons) -> Self {
        tuners.select(ActiveTuner::E2);

        Self { tuners, buttons }
    }

    /// Button press interrupt handler
    ///
    /// Must be called by the actual interrupt handler.
    ///
    /// # Arguments
    ///
    /// * `uptime` - Current system runtime used for button debouncing
    pub fn button_interrupt(&mut self, uptime: Duration) {
        let pressed = self.buttons.pressed(uptime);
        let active = self.tuners.active();

        let next = match pressed {
            Pressed::Up => match active {
                ActiveTuner::None => ActiveTuner::E2,
                ActiveTuner::E2 => ActiveTuner::E4,
                ActiveTuner::A2 => ActiveTuner::E2,
                ActiveTuner::D3 => ActiveTuner::A2,
                ActiveTuner::G3 => ActiveTuner::D3,
                ActiveTuner::H3 => ActiveTuner::G3,
                ActiveTuner::E4 => ActiveTuner::H3,
            },
            Pressed::Down => match active {
                ActiveTuner::None => ActiveTuner::E2,
                ActiveTuner::E2 => ActiveTuner::A2,
                ActiveTuner::A2 => ActiveTuner::D3,
                ActiveTuner::D3 => ActiveTuner::G3,
                ActiveTuner::G3 => ActiveTuner::H3,
                ActiveTuner::H3 => ActiveTuner::E4,
                ActiveTuner::E4 => ActiveTuner::E2,
            },
            _ => active
        };

        self.tuners.select(next);
    }
}

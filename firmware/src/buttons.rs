use core::time::Duration;
use stm32f1::stm32f103;
use cortex_m::asm;

// Minimum deadtime between button-presses for debouncing:
const DEADTIME: Duration = Duration::from_millis(100);

pub enum Pressed {
    None,
    Ok,
    Up,
    Down,
}

pub struct Buttons {
    exti: stm32f103::EXTI,
    gpioa: stm32f103::GPIOA,
    gpiob: stm32f103::GPIOB,
    last_press: core::time::Duration, 
}

impl Buttons {
    pub fn new(rcc: &stm32f103::RCC, afio: &stm32f103::AFIO, exti: stm32f103::EXTI,
               gpioa: stm32f103::GPIOA, gpiob: stm32f103::GPIOB) -> Self {

        // Configure peripheral clocks for GPIOA, GPIOB
        // and the Alternate function IO Peripherals.
        rcc.apb2enr.modify(|_, w| w.iopaen().enabled()
                           .iopben().enabled()
                           .afioen().enabled());

        // Setup all button GPIOs as inputs with internal
        // pullup resistors (Buttons are active low).
        gpioa.crh.modify(|_, w| w.mode15().input()
                         .cnf15().alt_push_pull());
        gpioa.odr.modify(|_, w| w.odr15().high());

        gpiob.crh.modify(|_, w| w.mode9().input()
                         .mode13().input()
                         .cnf9().alt_push_pull()
                         .cnf13().alt_push_pull());
        gpiob.odr.modify(|_, w| w.odr9().high()
                         .odr13().high());

        // Wait for the pull-ups to pull the lines high
        // (Charge capacitances etc.)
        asm::delay(80_000);

        // The interrupt handling for GPIOs is a bit complex.
        // Not every GPIO pin can be used as an interrupt input
        // at the same time. We have to choose which input from a
        // group of GPIOs we want to use.
        //
        // PA9,   PB9*,  PC9,  PD9  are in one group
        // PA13,  PB13*, PC13, PD13 are in one group
        // PA15*, PB15,  PC15, PD15 are in one group
        //
        // The calls below select the pins from the groups
        // denoted above by an *.
        unsafe {
            afio.exticr3.modify(|_, w| w.exti9().bits(0b0001)); // PB9
            afio.exticr4.modify(|_, w| w.exti13().bits(0b0001) // PB13
                                .exti15().bits(0b0000));// PA15
        }

        // Unmask (enable) the interrupts we use, ...
        exti.imr.modify(|_, w| w.mr9().unmasked()
                        .mr13().unmasked()
                        .mr15().unmasked());

        // ... generate interrupts on rising edges, ...
        exti.rtsr.modify(|_, w| w.tr9().enabled()
                         .tr13().enabled()
                         .tr15().enabled());

        // ... generate interrupts on falling edges.
        exti.ftsr.modify(|_, w| w.tr9().enabled()
                         .tr13().enabled()
                         .tr15().enabled());

        let last_press = Duration::new(0, 0); 
        
        Buttons { exti, gpioa, gpiob, last_press }
    }

    /// Get the currently pressed button
    ///
    /// If multipe buttons are pressed the button with the
    /// highest priority is reported.
    /// The priorities from highest to lowest are:
    /// 
    ///  * Ok
    ///  * Up
    ///  * Down
    pub fn pressed(&mut self, uptime: Duration) -> Pressed {
        let ia = self.gpioa.idr.read();
        let ib = self.gpiob.idr.read();

        // Clear all interrupt pending bits. If these
        // are not cleared when leaving the interrupt handler
        // it will be entered again right away, keeping the
        // MCU in a busy loop.
        self.exti.pr.modify(|_, w| w.pr9().clear()
                            .pr13().clear()
                            .pr15().clear());

        // Report no button pressed when we were called
        // no less than DEADTIME ago.
        if uptime < (self.last_press + DEADTIME) {
            return Pressed::None
        }

        if ib.idr13().is_low() {
            self.last_press = uptime;
            return Pressed::Ok;
        }

        if ib.idr9().is_low() {
            self.last_press = uptime;
            return Pressed::Up;
        }

        if ia.idr15().is_low() {
            self.last_press = uptime;
            return Pressed::Down;
        }

        Pressed::None
    }
}

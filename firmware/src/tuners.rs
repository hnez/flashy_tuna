use stm32f1::stm32f103;

#[derive(Clone, Copy)]
pub enum ActiveTuner {
    None,
    E2,
    A2,
    D3,
    G3,
    H3,
    E4,
}

pub struct Tuners {
    tim1: stm32f103::TIM1,
    tim2: stm32f103::TIM2,
    tim3: stm32f103::TIM3,
    tim4: stm32f103::TIM4,
    active: ActiveTuner,
}

impl Tuners {
    pub fn new(rcc: &stm32f103::RCC, gpioa: &stm32f103::GPIOA, gpiob: &stm32f103::GPIOB,
           tim1: stm32f103::TIM1, tim2: stm32f103::TIM2, tim3: stm32f103::TIM3,
           tim4: stm32f103::TIM4) -> Self {

        // Configure peripheral clocks
        rcc.apb1enr.modify(|_, w| w.tim2en().enabled()
                           .tim3en().enabled()
                           .tim4en().enabled());

        rcc.apb2enr.modify(|_, w| w.tim1en().enabled()
                           .iopaen().enabled()
                           .iopben().enabled());

        // CH1 - E4 - 329.63Hz
        tim1.ccmr1_output().modify(|_, w| w.oc1m().pwm_mode1());
        tim1.ccer.modify(|_, w| w.cc1e().set_bit().cc1p().set_bit());
        tim1.bdtr.modify(|_, w| w.moe().enabled());

        // CH2 - A2 - 110.00Hz | CH3 - D3 - 146.83Hz | CH4 - G3 - 196.00Hz
        tim2.ccmr1_output().modify(|_, w| w.oc2m().pwm_mode1());
        tim2.ccmr2_output().modify(|_, w| w.oc3m().pwm_mode1()
                                   .oc4m().pwm_mode1());
        tim2.ccer.modify(|_, w| w.cc2e().set_bit().cc2p().set_bit()
                         .cc3e().set_bit().cc3p().set_bit()
                         .cc4e().set_bit().cc4p().set_bit());

        // CH1 - H3 - 246.94Hz
        tim3.ccmr1_output().modify(|_, w| w.oc1m().pwm_mode1());
        tim3.ccer.modify(|_, w| w.cc1e().set_bit().cc1p().set_bit());

        // CH1 - E4 - 82.41Hz
        tim4.ccmr1_output().modify(|_, w| w.oc1m().pwm_mode1());
        tim4.ccer.modify(|_, w| w.cc1e().set_bit().cc1p().set_bit());

        // Run all timers on a 4MHz base-clock
        tim1.cr1.modify(|_, w| w.ckd().div2()
                        .cen().enabled());

        tim2.cr1.modify(|_, w| w.ckd().div2()
                        .cen().enabled());

        tim3.cr1.modify(|_, w| w.ckd().div2()
                        .cen().enabled());

        tim4.cr1.modify(|_, w| w.ckd().div2()
                        .cen().enabled());

        // Configure all Pins to be controlled by the timers
        gpioa.crl.modify(|_, w| w.mode1().output().cnf1().alt_push_pull()
                         .mode2().output().cnf2().alt_push_pull()
                         .mode3().output().cnf3().alt_push_pull()
                         .mode6().output().cnf6().alt_push_pull());

        gpioa.crh.modify(|_, w| w.mode8().output().cnf8().alt_push_pull());

        gpiob.crl.modify(|_, w| w.mode6().output().cnf6().alt_push_pull());

        let active = ActiveTuner::None;

        Self { tim1, tim2, tim3, tim4, active }
    }

    pub fn select(&mut self, active: ActiveTuner) {
        self.tim1.ccr1.write(|w| w.ccr().bits(0));
        self.tim2.ccr2.write(|w| w.ccr().bits(0));
        self.tim2.ccr3.write(|w| w.ccr().bits(0));
        self.tim2.ccr4.write(|w| w.ccr().bits(0));
        self.tim3.ccr1.write(|w| w.ccr().bits(0));
        self.tim4.ccr1.write(|w| w.ccr().bits(0));

        match active {
            ActiveTuner::E2 => {
                self.tim4.ccr1.write(|w| w.ccr().bits(4854));
                self.tim4.arr.write(|w| w.arr().bits(48538));
            },
            ActiveTuner::A2 => {
                self.tim2.ccr2.write(|w| w.ccr().bits(3636));
                self.tim2.arr.write(|w| w.arr().bits(36364));
            },
            ActiveTuner::D3 => {
                self.tim2.ccr3.write(|w| w.ccr().bits(2724));
                self.tim2.arr.write(|w| w.arr().bits(27242));
            },
            ActiveTuner::G3 => {
                self.tim2.ccr4.write(|w| w.ccr().bits(2041));
                self.tim2.arr.write(|w| w.arr().bits(20408));
            },
            ActiveTuner::H3 => {
                self.tim3.ccr1.write(|w| w.ccr().bits(1620));
                self.tim3.arr.write(|w| w.arr().bits(16198));
            },
            ActiveTuner::E4 => {
                self.tim1.ccr1.write(|w| w.ccr().bits(1213));
                self.tim1.arr.write(|w| w.arr().bits(12135));

            },
            ActiveTuner::None => (),
        }

        self.active = active;
    }

    pub fn active(&self) -> ActiveTuner {
        self.active
    }
}

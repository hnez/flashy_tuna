#![no_std]
#![no_main]

use panic_halt as _;
use cortex_m::asm::wfi;
use rtic::app;

mod tuners;
mod buttons;
mod ui;
mod uptime;

#[app(device = stm32f1::stm32f103, peripherals = true)]
const APP: () = {
    // Runtime resources
    //
    // All resources that should be available to tasks during
    // runtime must be included in this struct.
    struct Resources {
        uptime: uptime::Uptime,
        ui: ui::Ui,
    }

    /// System setup function
    ///
    /// The init function has access to all hardware resources of
    /// the system. All resources that should be reached later in
    /// the execution have to be put into the Resources struct above.
    ///
    /// Interrupts are disabled in the init function and enabled
    /// after returning from it.
    #[init]
    fn init(cx: init::Context) -> init::LateResources {
        // Peripherals defined by ARM for the
        // Cortex M family of MCUs
        let core = cx.core;        
        let syst = core.SYST;

        // Peripherals defined by ST for the
        // STM32F1 family of MCUs.
        let device = cx.device;
        let afio = device.AFIO;
        let gpioa = device.GPIOA;
        let gpiob = device.GPIOB;
        let exti = device.EXTI;
        let rcc = device.RCC;
        let tim1 = device.TIM1;
        let tim2 = device.TIM2;
        let tim3 = device.TIM3;
        let tim4 = device.TIM4;
        
        // Setup the user interface ...
        let ui = {
            // .. which includes the LEDs used as tuners ...
            let tuners = tuners::Tuners::new(
                &rcc, &gpioa, &gpiob, tim1, tim2, tim3, tim4
            );

            // .. and the buttons to control the interface.
            let buttons = buttons::Buttons::new(
                &rcc, &afio, exti, gpioa, gpiob
            );

            ui::Ui::new(tuners, buttons)
        };

        // Setup a timekeeper that keeps track of
        // the system runtime
        let uptime = uptime::Uptime::new(syst);

        // Make the initialized resources available
        // to other tasks.
        init::LateResources { ui, uptime }
    }

    /// Main loop
    ///
    /// User intection is handled using interrupts and
    /// the LED control is performed using timer peripherals.
    /// The only thing left to do in the main loop is to
    /// (re-)enter a low-power state whenever an interrupt occurs.
    #[idle]
    fn idle(_cx: idle::Context) -> ! {
        loop {
            // wfi pauses program execution until an interrupt occurs.
            // The system also enters a lower-power state when waiting
            // for an interrupt.
            wfi();
        }
    }

    /// Interrupt handler triggered by the SysTick Timer
    ///
    /// The SysTick timer is configured to be triggered
    /// once per second in the uptime module.
    /// The uptime module keeps track of the absolute time
    /// the program is running.
    #[task(binds = SysTick, resources = [uptime])]
    fn systick(cx: systick::Context) {
        cx.resources.uptime.step_seconds();
    }

    /// Interrupt handler triggered by External (GPIO)
    /// interrupts 5..9.
    ///
    /// We use PB9 as interrupt pin for a button.
    /// Interrupts for this pin are routed here using
    /// the EXTI peripheral configuration.
    #[task(binds = EXTI9_5, resources = [uptime, ui])]
    fn exti9_5(cx: exti9_5::Context) {
        // The current uptime is used for button debouncing
        let uptime = cx.resources.uptime.now();
        cx.resources.ui.button_interrupt(uptime);
    }

    /// Interrupt handler triggered by External (GPIO)
    /// interrupts 10..15.
    ///
    /// We use PB13 and PA15  as interrupt pins for buttons.
    /// Interrupts for these pins are routed here using
    /// the EXTI peripheral configuration.
    #[task(binds = EXTI15_10, resources = [uptime, ui])]
    fn exti15_10(cx: exti15_10::Context) {
        // The current uptime is used for button debouncing
        let uptime = cx.resources.uptime.now();
        cx.resources.ui.button_interrupt(uptime);
    }
};

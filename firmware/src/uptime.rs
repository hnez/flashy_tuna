use core::time::Duration;

use cortex_m::peripheral::syst::SystClkSource;
use stm32f1::stm32f103;

const NS_PER_SYSTICK : u32 = 1_000;
const SYSTICK_PER_S : u32 = 1_000_000;

pub struct Uptime {
    seconds: u64,
}

impl Uptime {
    pub fn new(mut syst: stm32f103::SYST) -> Self {
        // Setup the SysTick core peripheral to
        // generate interrupts at a frequency of 1Hz
        syst.disable_counter();
        syst.set_clock_source(SystClkSource::Core);
        syst.set_reload(SYSTICK_PER_S);
        syst.clear_current();
        syst.enable_interrupt();
        syst.enable_counter();

        Self {
            seconds: 0,
        }
    }

    /// SysTick interrupt handler
    ///
    /// Must be called by the actual interrupt handler.
    pub fn step_seconds(&mut self) {
        self.seconds += 1;
    }

    /// Current system uptime
    pub fn now(&self) -> Duration {
        let nanos = NS_PER_SYSTICK * stm32f103::SYST::get_current();

        Duration::new(self.seconds, nanos)
    }
}

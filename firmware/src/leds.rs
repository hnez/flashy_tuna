use stm32f1::stm32f103;
use core::marker::PhantomData;

pub enum ActiveTuner {
    Off,
    E2,
    A2,
    D3,
    G3,
    H3,
    E4,
}

pub struct Tuners {
    tim1: stm32f103::TIM1,
    tim2: stm32f103::TIM2,
    tim3: stm32f103::TIM3,
    tim4: stm32f103::TIM4,
}

impl Tuners {
    fn new(rcc: &stm32f103::RCC, gpioa: &stm32f103::GPIOA, gpiob: &stm32f103::GPIOB,
           tim1: stm32f103::TIM1, tim2: stm32f103::TIM2, tim3: stm32f103::TIM3,
           tim4: stm32f103::TIM4) -> Self {

        rcc.apb1enr.modify(|_, w| w.tim2en().enabled()
                           .tim3en().enabled()
                           .tim4en().enabled());

        // CH1 - E4 - 329.63Hz
        tim1.ccmr1_output().modify(|_, w| w.oc1m().pwm_mode2());

        // CH2 - A2 - 110.00Hz | CH3 - D3 - 146.83Hz | CH4 - G3 - 196.00Hz
        tim2.ccmr1_output().modify(|_, w| w.oc2m().pwm_mode2());
        tim2.ccmr2_output().modify(|_, w| w.oc3m().pwm_mode2()
                                   .oc4m().pwm_mode2());

        // CH1 - H3 - 246.94Hz
        tim3.ccmr1_output().modify(|_, w| w.oc1m().pwm_mode2());

        // CH1 - E4 - 329.63Hz
        tim4.ccmr1_output().modify(|_, w| w.oc1m().pwm_mode2());

        // Run all timers on a 4MHz base-clock
        tim1.cr1.modify(|_, w| w.ckd().div2()
                        .cen().enabled());

        tim2.cr1.modify(|_, w| w.ckd().div2()
                        .cen().enabled());

        tim3.cr1.modify(|_, w| w.ckd().div2()
                        .cen().enabled());

        tim4.cr1.modify(|_, w| w.ckd().div2()
                        .cen().enabled());


        rcc.apb2enr.modify(|_, w| w.iopaen().enabled()
                           .iopben().enabled());

        gpioa.crl.modify(|_, w| w.mode1().alt_push_pull()
                         .mode2().alt_push_pull()
                         .mode3().alt_push_pull()
                         .mode6().alt_push_pull());
        gpioa.crh.modify(|_, w| w.mode8().alt_push_pull());
        gpioa.odr.modify(|_, w| w.odr1().high()
                         .odr2().high()
                         .odr3().high()
                         .odr6().high()
                         .odr8().high());

        gpiob.crl.modify(|_, w| w.mode6().alt_push_pull());
        gpiob.odr.modify(|_, w| w.odr6().high());

        Self {
            gpioa: gpioa,
            gpiob: gpiob,
            active: PhantomData,
        }
    }

    fn select(&self, active: ActiveTuner) {
        self.tim1.ccr1.write(|w| w.ccr().bits(0));
        self.tim2.ccr2.write(|w| w.ccr().bits(0));
        self.tim2.ccr3.write(|w| w.ccr().bits(0));
        self.tim2.ccr4.write(|w| w.ccr().bits(0));
        self.tim3.ccr1.write(|w| w.ccr().bits(0));
        self.tim4.ccr1.write(|w| w.ccr().bits(0));

        match active {
            ActiveTuner::E2 => {
                self.tim4.ccr1.write(|w| w.ccr().bits(1213));
                self.tim4.arr.write(|w| w.arr().bits(12135));
            },
            ActiveTuner::A2 => {
                self.tim2.ccr2.write(|w| w.ccr().bits(1620));
                self.tim2.arr.write(|w| w.arr().bits(16198));
            },
            ActiveTuner::D3 => {
                self.tim2.ccr3.write(|w| w.ccr().bits(2041));
                self.tim2.arr.write(|w| w.arr().bits(20408));
            },
            ActiveTuner::G3 => {
                self.tim2.ccr4.write(|w| w.ccr().bits(2724));
                self.tim2.arr.write(|w| w.arr().bits(27242));
            },
            ActiveTuner::H3 => {
                self.tim3.ccr1.write(|w| w.ccr().bits(3636));
                self.tim3.arr.write(|w| w.arr().bits(36364));
            },
            ActiveTuner::E4 => {
                self.tim1.ccr1.write(|w| w.ccr().bits(4854));
                self.tim1.arr.write(|w| w.arr().bits(48538));
            },
            ActiveTuner::Off => {},
        }
    }
}

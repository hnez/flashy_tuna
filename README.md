Flashy Tuna
===========

![Flashy Tuna](flash_tuna.png)

This is the flashy tuna, a fun little weekend project
that tunes a guitar using six flashing LEDs and
the stroboscopic effect.

The project consists of a firmware portion and a hardware
portion, organized into their respective directories.
